using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public Text tiempo;

    private void Start()
    {

        float mejorTiempo = PlayerPrefs.GetFloat("mejorTiempo", 0);
        float tiempoPartida = PlayerPrefs.GetFloat("tiempoPartida", 0);

        if (mejorTiempo < tiempoPartida)
        {
            mejorTiempo= tiempoPartida;
            PlayerPrefs.SetFloat("mejorTiempo", mejorTiempo);
        }

        tiempo.text = mejorTiempo.ToString("F2");
    }

    public void CambiarEscena()
    {
        SceneManager.LoadScene(1);
    }
}
