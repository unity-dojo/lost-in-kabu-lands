using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoPersonaje : MonoBehaviour
{
    public AudioManager audio;
    public GameObject prefabProyectil;
    public Transform ca�on;
    public Transform puntaCa�on;

    void Update()
    {
        Apuntar();
    }

    /// <summary>
    /// Apunta al punto donde est� el cursor
    /// </summary>
    void Apuntar()
    {
        Vector3 raton = Input.mousePosition; // Cogemos el punto del cursor en la pantalla (es un Vector2 (x,y))
        raton.z = 20; // Seteamos la Z a la distancia a la que est� la c�mara del suelo
        Vector3 puntoMundo3D = Camera.main.ScreenToWorldPoint(raton); // Convertimos el vector al mundo 3D
        transform.GetChild(1).LookAt(puntoMundo3D); // Rotamos el ca��n para apuntar a ese punto

        if (Input.GetButtonDown("Fire1"))
        {
            Instantiate(prefabProyectil, puntaCa�on.position, ca�on.rotation);
            audio.ReproducirDisparo();
        }
    }
}
