using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorEnemigos : MonoBehaviour
{

    public Transform eje;
    public Transform puntoInstancia;
    public GameObject prefabEnemigo;

    public float velocidadAngular=10F;
    public float frecuencia=1F;

    bool activo = false;

    private void Start()
    {
        Activar();
    }

    private void Update()
    {
        if (activo)
        {
            Rotar();
        }
    }

    void Rotar()
    {
        eje.Rotate(0, velocidadAngular * Time.deltaTime, 0);
    }

    void Instanciar()
    {
        Instantiate(prefabEnemigo, puntoInstancia.position, Quaternion.identity);
    }

    public void Activar()
    {
        activo = true;
        InvokeRepeating("Instanciar",0,frecuencia);

    }

    public void Desactivar()
    {
        activo = false;
        CancelInvoke();
    }

    public void CambiarFrecuencia()
    {

    }

    public void CambiarVelocidad()
    {

    }
}
